package com.example.capson;

import static org.junit.Assert.assertNotNull;

import java.util.Optional;

import org.junit.jupiter.api.ClassOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.annotation.Order;

import com.example.capson.Exception.NotFoundException;
import com.example.capson.entities.Customer;
import com.example.capson.repositories.CustomerRepo;
import org.junit.jupiter.*;


@SpringBootTest
class CapsonProjectApplicationTests {

	@Autowired
	CustomerRepo cusrepo;
	
	@Test
	@Order(1)
	void testSaveCustomer()
	{
		System.out.println("In save customer test case ");
		Customer customer = new Customer(1,"Arpan Das","Arpan@wipro.com","541152452");
		cusrepo.save(customer);
	}
	@Test 
	@Order(2)
	void testGetCustomer()
	{
		Optional<Customer> findcustomer = cusrepo.findById(1);
		if(findcustomer.isPresent())
		{
			Customer customer = findcustomer.get();
			System.out.println("Customer Details : "+customer.getCustomerName());
		}
		
	}
	@Test
	@Order(3)
	void testupdateCustomer()
	{
		Optional<Customer> findcustomer = cusrepo.findById(1);
		if(findcustomer.isPresent())
		{
			Customer customer = findcustomer.get();
			customer.setCustomerEmail("arpandas@wipro.com");
			cusrepo.save(customer);
		}
	}
	@Test
	@Order(4)
	void deleteCustomer()
	{
		Optional<Customer> findcustomer = cusrepo.findById(531351);
		if(findcustomer.isPresent())
		{
			cusrepo.delete(findcustomer.get());
			System.out.println("Customer Deleted sSuccessfully");
		}
	}
	
}
