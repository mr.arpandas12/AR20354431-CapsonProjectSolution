package com.example.capson.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="CUSTOMER")
public class Customer {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int customerId;
	private String CustomerName;
	private String CustomerEmail;
	private String CustomenrPhoneno;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable
	(
			name = "customer_accounts",
			joinColumns = @JoinColumn(name = "customer_id"),
			inverseJoinColumns = @JoinColumn(name = "AccountNo")
	)
	private Set<Account> accounts;
	public Set<Account> getAccounts() {
		return accounts;
	}
	public void setAccounts(Set<Account> accounts) {
		this.accounts = accounts;
	}
	public Customer()
	{
		
	}
	public Customer(int customerId,String CustomerName, String CustomerEmail , String CustomenrPhoneno)
	{
		this.CustomerEmail = CustomerEmail;
		this.customerId = customerId;
		this.CustomerName = CustomerName;
		this.CustomenrPhoneno = CustomenrPhoneno;
	}

	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return CustomerName;
	}
	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}
	public String getCustomerEmail() {
		return CustomerEmail;
	}
	public void setCustomerEmail(String customerEmail) {
		CustomerEmail = customerEmail;
	}
	public String getCustomenrPhoneno() {
		return CustomenrPhoneno;
	}
	public void setCustomenrPhoneno(String customenrPhoneno) {
		CustomenrPhoneno = customenrPhoneno;
	}
}