package com.example.capson.entities;
import com.example.capson.entities.*;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
@Entity
@Table(name="Account")
public class Account {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int AccountNo;
	private String AccountType;
	private double balance;
	
	@JsonIgnore
	@ManyToMany(cascade = CascadeType.ALL)
	private Set<Customer> customers;

	public Account()
	{
		
	}
	
	public Account(int Accountno , int CustomerId , String AccountType , double balance)
	{
		super();
		this.AccountNo = Accountno;
		this.AccountType = AccountType;
		this.balance = balance;
	}

	
	public Set<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(Set<Customer> customers) {
		this.customers = customers;
	}

	public int getAccountNo() {
		return AccountNo;
	}
	public void setAccountNo(int accountNo) {
		AccountNo = accountNo;
	}
	public String getAccountType() {
		return AccountType;
	}
	public void setAccountType(String accountType) {
		AccountType = accountType;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}

}