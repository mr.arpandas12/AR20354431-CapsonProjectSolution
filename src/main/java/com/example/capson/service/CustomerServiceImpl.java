package com.example.capson.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.capson.Exception.NotFoundException;
import com.example.capson.entities.Account;
import com.example.capson.entities.Customer;
import com.example.capson.repositories.AccountRepo;
import com.example.capson.repositories.CustomerRepo;


@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private AccountRepo acntrepo;
	
	@Autowired
	private CustomerRepo cusrepo;

	
	
//--------------------------------- GET ALL CUSTOMER --------------------------------
	@Override
	public List<Customer> getAllCustomer() {

		List<Customer> customersALL = (List<Customer>) cusrepo.findAll();
		return customersALL;
	}

//--------------------------------- GET ALL ACCOUNTS --------------------------------

	@Override
	public List<Account> getAllAccounts() {
		List<Account> accountsALL = (List<Account>) acntrepo.findAll();
		return accountsALL;
	}

//--------------------------------- GET ACCOUNT BALANCE --------------------------------
	@Override
	public double getAccountbalance(int AccountNo) {
		
		java.util.Optional<Account> findAccount = acntrepo.findById(AccountNo);
		Account existAccount = null;
		if(findAccount.isPresent())
		{
			existAccount = findAccount.get();
		}
		if(existAccount == null)
		{
			throw new NotFoundException("Account","Id",AccountNo);
		}
		return existAccount.getBalance();
	}

//--------------------------------- GET ACCOUNT DETAIL --------------------------------
	@Override
	public Account getAccountdetail(int AccountNo) {

		java.util.Optional<Account> findAccount = acntrepo.findById(AccountNo);
		Account existAccount = null;
		if(findAccount.isPresent())
		{
			existAccount = findAccount.get();
		}
		if(existAccount == null)
		{
			throw new NotFoundException("Account","Id",AccountNo);
		}
		return existAccount;
	}
	
//--------------------------------- GET CUSTOMER DETAIL--------------------------------
	@Override
	public Customer getCustomerDetail(int customerId) {
	java.util.Optional<Customer> findCust = cusrepo.findById(customerId);
	Customer existCustomer = null;
	if(findCust.isPresent())
	{
		existCustomer = findCust.get();
	}
	if(existCustomer==null)
	{
		throw new NotFoundException("Customer","Id",customerId);
	}
	
	return existCustomer;
	}
	//--------------------------------- TRANSFER FUNDS--------------------------------
	@Override
	public String Fundtransfer(int From, int to, double Balance) {
		
		if(From==to)
			throw new NotFoundException("From Account and To Account should not be the same !!!");
		int AccountNo = From;
		Optional<Account> findfromAccount = acntrepo.findById(AccountNo);
		if(findfromAccount.isEmpty())
			throw new NotFoundException("Account No. is Invalid");
		Account fromAccount = findfromAccount.get();
		
		AccountNo = to;
		Optional<Account> findtoAccount = acntrepo.findById(AccountNo);
		if(findtoAccount.isEmpty())
			throw new NotFoundException("Account No. is Invalid");
		Account toAccount = findtoAccount.get();
		
		
		if(fromAccount.getAccountType() != toAccount.getAccountType())
			throw new NotFoundException("Both Account should have same type !!! ");
		if(fromAccount.getBalance()<Balance)
			throw new NotFoundException("Insufficient Balance");
		else if(fromAccount.getBalance()>Balance)
		{
			fromAccount.setBalance(fromAccount.getBalance()-Balance);
			toAccount.setBalance(toAccount.getBalance()+Balance);
			acntrepo.save(fromAccount);
			acntrepo.save(toAccount);
		}
		return "Balance transferred successfully!!!";
	}

//--------------------------------- SAVE CUSTOMER --------------------------------
	@Override
	public Customer saveCustomer(Customer customer) {
	
		return cusrepo.save(customer);
	}
//--------------------------------- SAVE ACCOUNT --------------------------------
	@Override
	public Account saveAccount(Account acnt) {
	
		return acntrepo.save(acnt);
	}
//--------------------------------- UPDATE CUSTOMER --------------------------------
	@Override
	public Customer UpdateCustomerDetail(int CustomerId, Customer customer) {
		java.util.Optional<Customer> findCust = cusrepo.findById(CustomerId);
		Customer existCustomer = null;
		if(findCust.isPresent())
		{
			existCustomer = findCust.get();
			existCustomer.setCustomenrPhoneno(customer.getCustomenrPhoneno());
			existCustomer.setCustomerEmail(customer.getCustomerEmail());
			existCustomer.setCustomerName(customer.getCustomerName());
		}
		if(existCustomer==null)
		{
			throw new NotFoundException("Customer","Id",CustomerId);
		}
		
		return cusrepo.save(existCustomer);
	}
	//--------------------------------- DELETE CUSTOMER --------------------------------
	@Override
	public String deleteCustomer(int customerId) {
		java.util.Optional<Customer> findCust = cusrepo.findById(customerId);
		Customer existCustomer = null;
		if(findCust.isPresent())
		{
			existCustomer = findCust.get();
		}

		if(existCustomer==null)
		{
			throw new NotFoundException("Customer","Id",customerId);
		}
		cusrepo.delete(existCustomer);
		
		return "Customer ID : "+customerId+" deleted Successfully!!";
	}

	@Override
	public String deleteAccount(int AccountNo) {
	
		Optional<Account> findacnt = acntrepo.findById(AccountNo);
		Account existAccount = null;
		if(findacnt.isPresent())
		{
			existAccount = findacnt.get();
		}

		if(existAccount==null)
		{
			throw new NotFoundException("Account","Id",AccountNo);
		}
		acntrepo.delete(existAccount);
		
		return "AccountNo. : "+AccountNo+" deleted Successfully!!";
	}

	@Override
	public Customer assignedNewAccount(int customerId, int AccountNo) {
		
		Set<Account> accounts = null;
		Customer customer = cusrepo.findById(customerId).get();
		Account account = acntrepo.findById(AccountNo).get();
		accounts = customer.getAccounts();
		accounts.add(account);
		customer.setAccounts(accounts);
		return cusrepo.save(customer);
		
		
	}


}
