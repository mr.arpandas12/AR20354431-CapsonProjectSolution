package com.example.capson.service;

import java.util.List;

import com.example.capson.entities.Account;
import com.example.capson.entities.Customer;

public interface CustomerService {
	public List<Customer> getAllCustomer();
	public List<Account> getAllAccounts();
	public double getAccountbalance(int accountNo);
	public Account getAccountdetail(int accountNo);
	public Customer getCustomerDetail(int customerid);
	public String Fundtransfer(int From , int to , double Blanace);
	public Customer saveCustomer(Customer cus);
	public Account saveAccount(Account acnt);
	public Customer UpdateCustomerDetail(int CustomerId , Customer customer);
	public String deleteCustomer(int customerId);
	public String deleteAccount(int AccountNo);
	public Customer assignedNewAccount(int customerId , int AccountNo);	

}
