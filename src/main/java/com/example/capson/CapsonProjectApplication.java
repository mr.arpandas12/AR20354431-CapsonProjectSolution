package com.example.capson;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CapsonProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(CapsonProjectApplication.class, args);
	}

}
