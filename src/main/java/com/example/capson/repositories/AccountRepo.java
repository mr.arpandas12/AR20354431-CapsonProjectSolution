package com.example.capson.repositories;



import org.springframework.data.repository.CrudRepository;

import com.example.capson.entities.Account;



public interface AccountRepo extends CrudRepository<Account,Integer> {

}
