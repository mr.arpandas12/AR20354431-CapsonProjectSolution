package com.example.capson.repositories;

import com.example.capson.entities.Customer;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepo extends CrudRepository<Customer,Integer>{

}
