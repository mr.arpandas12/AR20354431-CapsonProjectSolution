package com.example.capson.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.capson.entities.Account;
import com.example.capson.entities.Customer;
import com.example.capson.service.CustomerService;

@RestController
public class Controller {

	@Autowired
	private CustomerService service;

	@PostMapping("/api/customers/save")
	public Customer saveCustomer(@RequestBody Customer customer)
	{
		Customer cust = service.saveCustomer(customer);
		return cust;
	}
	@PostMapping("/api/accounts/save")
	public Account saveAccount(@RequestBody Account account)
	{
		Account acnt = service.saveAccount(account);
		return acnt;
	}
	@GetMapping("/api/customers/{id}")
	public ResponseEntity<Customer> findCustomer(@PathVariable("id") int customerId)
	{
		Customer customer = service.getCustomerDetail(customerId);
		return new ResponseEntity<Customer>(service.getCustomerDetail(customerId) , HttpStatus.OK);
	}
	
	@GetMapping("/api/accounts/{id}")
	public Account findAccount(@PathVariable("id") int accountNo)
	{
		Account account = service.getAccountdetail(accountNo);
		return account;
	}
	@GetMapping("/api/getbalance/{id}")
	public String findAccountBalance(@PathVariable("id") int accountNo)
	{
		double balance = service.getAccountbalance(accountNo);
		return "Account balance of "+accountNo+" is : "+balance;
	}
	@RequestMapping(value="/api/customers",method = RequestMethod.GET)
	public List<Customer> getAllcustomer()
	{
		List<Customer> customers = service.getAllCustomer();
		return customers;
	}
	
	@RequestMapping(value="/api/accounts",method = RequestMethod.GET)
	public List<Account> getAllAccounts()
	{
		List<Account> accounts = service.getAllAccounts();
		return accounts;
	}
	@PutMapping("/api/customers/update/{id}")
	public Customer UpdateCustomer(@PathVariable("id") int customerId, @RequestBody Customer customer)
	{
		return service.UpdateCustomerDetail(customerId, customer);
	}
	@RequestMapping(value = "api/transferAmopunt/{from}/{to}/{balance}",method = RequestMethod.POST)
	public String transferFund(@PathVariable int from,@PathVariable int to , @PathVariable double balance )
	{
		return service.Fundtransfer(from, to, balance);
	}
	@DeleteMapping("/api/customers/delete/{id}")
	public String DeleteCustomer(@PathVariable("id") int CustomerID)
	{
		return service.deleteCustomer(CustomerID);
	}
	@DeleteMapping("/api/accounts/delete/{id}")
	public String DeleteAccount(@PathVariable("id") int AccountNo)
	{
		return service.deleteAccount(AccountNo);
	}
	
	@PutMapping("api/{customerId}/account/{accountNo}")
	public Customer assignedNewAccount(@PathVariable int customerId , @PathVariable int accountNo)
	{
		return service.assignedNewAccount(customerId, accountNo);
	}
}


